install.packages("MASS")
library("MASS")
data(Boston)
Boston

mean <- function(x) {sum(x)/length(x)}

start <- Sys.time()
lapply(Boston,mean)
end <- Sys.time() - start
print(end)

x <- sample(x = 1:1000, size = 1000000, replace = TRUE) 
y <- sample(x = 1:1000, size = 1000000, replace = TRUE)
z <- sample(x = 1:1000, size = 1000000, replace = TRUE)
data <- data.frame(x, y, z)

start <- Sys.time()
lapply(data,mean)
end <- Sys.time() - start
print(end)

start <- Sys.time()
mclapply(data,mean,mc.cores=1)
end <- Sys.time() - start
print(end)


lapply(Boston,min)
lapply(Boston,max)
lapply(Boston,median)
lapply(Boston,range)
lapply(Boston,sd)
lapply(Boston,var)
sapply(Boston,IQR)
sapply(Boston,quantile)
summary(Boston)

plot(Boston)
boxplot(Boston)
hist(as.numeric(Boston[,1]),main="Histogram of crim column",xlab="crim")